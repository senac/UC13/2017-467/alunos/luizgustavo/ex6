/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.br;

import br.com.senac.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class Teste {
    
    public Teste() {
    }
    @Test
    public void alunoAprovado(){
        Principal p = new Principal();
        double nota = 10;
        String result;
        result = p.situacaoAluno(nota);
        
        assertEquals("Aprovado", result);
        
    }
    @Test
    public void alunoReprovado(){
        Principal p = new Principal();
        double nota = 2;
        String result;
        result = p.situacaoAluno(nota);
        
        assertEquals("Reprovado", result);
    }
    @Test
    public void alunoRecuperacao(){
        Principal p = new Principal();
        double nota = 10;
        String result;
        result = p.situacaoAluno(nota);
        
        assertEquals("Recuperação",result);
    }
    
}
