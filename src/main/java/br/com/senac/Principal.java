/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/* Crie um programa que determine se um dado aluno esta: aprovado , em recuperação
ou aprovado para os seguintes critérios abaixo :
Nota >7 -> aprovado , Nota entre 4 e 6 recuperação abaixo de 4 reprovado.
Obs: lembre-se da orientação a objeto e da coesão .

 */
public class Principal {
    
    public String situacaoAluno(double nota){
        String resultado;
        if (nota > 7 ){
            resultado = "Aprovado";   
        }else if(nota < 4){
            resultado = "Reprovado";
        }else{
            resultado = "Recuperação";
        }
    return resultado;
    }
    
}
